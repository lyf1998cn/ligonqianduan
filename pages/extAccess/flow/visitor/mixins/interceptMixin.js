export default function() {
	return {
		methods: {
			handleNotWechatMpLogin: function() {
				if(!this.isWechatMpLogin()) {
					let title = "当前不是从微信公众号单点登录！";
					let duration = 3000;
					uni.showToast({
						title: title,
						icon: 'none',
						mask: true,
						duration: duration
					})
					window.setTimeout(() => {
						let navigateBackUrl = "pages/extAccess/workbeach/index";
						this.$u.route({
							url:navigateBackUrl
						});
					},2000);
				}
			}
		}
	}
}
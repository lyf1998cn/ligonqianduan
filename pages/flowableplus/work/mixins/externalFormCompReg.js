/**
 * 自定义外置表单的组件化注册
 *
 */
export const externalFormCompReg = {
  computed: {
    externalFormComponents: function () {
      return [
        // {
        //   text: '测试单',
        //   routeKey: 'demo',
        //   component: () => import('@/pages/flowableplus/work/external/demo/form.vue'),
        //   businessTable: 'fjut_process_demo'
        // },
        // {
        //   text: '政府采购预算申请',
        //   routeKey: 'purchase',
        //   component: () => import('@/pages/flowableplus/work/external/purchase/form.vue'),
        //   businessTable: 'fjut_process_purchase'
        // },
        {
          text: '访客预约申请',
          routeKey: 'visitor',
          component: () => import('@/pages/flowableplus/work/external/visitor/form.vue'),
          businessTable: 'fjut_process_visitor'
        },
		{
		  text: '企微群申请',
		  routeKey: 'scrmgroup',
		  component: () => import('@/pages/flowableplus/work/external/scrmgroup/form.vue'),
		  businessTable: 'fjut_process_group'
		}
      ]
    }
  },
  methods: {
    getExternalFormComponent(routeKey) {
      return this.externalFormComponents.find((element) => {
        return element.routeKey === routeKey
      }) || {};
    }
  }
}
export default function() {
	return {
		data() {
			/**
			 * formRef: 表单ref
			 * formData：表单数据
			 *
			 */
			return {
				formRef: 'form',
				formData: {

				},
				flowData: {
					currentUser: {}
				},
				disabled: this.disabledProp,
			}
		},
		/**
		 * @property {String} processDefinitionId 流程定义ID
		 * @property {Object} formDataProp 表单数据对象
		 * @property {Boolean} disabled 是否禁用表单，默认为false
		 * @property {Boolean} formBtns 是否展示表单按钮，默认为true
		 * @property {Object} flowDataProp 流程流转数据
		 */
		props: {
			/**
			 * 流程定义ID
			 */
			processDefinitionIdProp: {
				type: String
			},
			/**
			 * 表单数据对象
			 */
			formDataProp: {
				type: Object,
				default: () => ({})
			},
			/**
			 * 是否禁用表单，默认为false
			 */
			disabledProp: {
				type: Boolean,
				default: false
			},
			/**
			 *  是否展示表单按钮，默认为true
			 */
			formBtns: {
				type: Boolean,
				default: true
			},
			/**
			 * 当前流程流转信息
			 */
			flowDataProp: {
				type: Object,
				default: () => ({})
			}
		},
		created() {
			// created钩子在实例创建完成后被立即同步调用
			// 回填表单数据
			this.formData = Object.assign({}, this.formData, this.formDataProp);
			this.flowData = Object.assign({}, this.flowData, this.flowDataProp);
			// console.log("mixin中 created.","this.formData",JSON.stringify(this.formData));
		}
	}
}
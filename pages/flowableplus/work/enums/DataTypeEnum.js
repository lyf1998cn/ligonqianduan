/**
 * 获取枚举值：DATATYPE_MAP.CANDIDATEUSERS_API_GET
 * 获取枚举名称：DATATYPE_MAP.getName('CANDIDATEUSERS_API_GET')
 * 获取枚举描述：DATATYPE_MAP.getDesc('CANDIDATEUSERS_API_GET')
 * 通过枚举值获取描述：DATATYPE_MAP.getDescFromValue('CANDIDATEUSERS_API_GET')
 */
let DATATYPE_MAP = createEnum({
  USERS: ["USERS", '指定用户', '指定用户'],
  ROLES: ["ROLES", '角色', '角色'],
  DEPTS: ["DEPTS", '部门', '部门'],
  INITIATOR: ["INITIATOR", '发起人', '发起人'],
  CANDIDATEUSERS_API_GET: ["CANDIDATEUSERS_API_GET", '候选用户', '候选用户的数据源通过API GET请求获取'],
});

function createEnum(definition) {
  // 枚举值
  const valueMap = {};
  // 枚举名称
  const nameMap = {};
  // 枚举描述
  const descMap = {};
  for (const key of Object.keys(definition)) {
    const [value, name, desc] = definition[key];
    valueMap[key] = value;
    nameMap[value] = name;
    descMap[value] = desc;
  }
  return {
    ...valueMap,
    getName(key) {
      return (definition[key] && definition[key][1]) || '未定义';
    },
    getDesc(key) {
      return (definition[key] && definition[key][2]) || '未定义';
    },
    getNameFromValue(value) {
      return nameMap[value] || '未定义';
    },
    getDescFromValue(value) {
      return descMap[value] || '未定义';
    }
  }
}

export default DATATYPE_MAP;

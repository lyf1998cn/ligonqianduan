// 回显数据字典
export function selectDictLabel(datas, value) {
  if (value === undefined) {
    return "";
  }
  var actions = [];
  Object.keys(datas).some((key) => {
    if (datas[key].dictKey == ('' + value)) {
      actions.push(datas[key].dictValue);
      return true;
    }
  })
  if (actions.length === 0) {
    actions.push(value);
  }
  return actions.join('');
}
export function selectDictValue(datas, label) {
  if (label === undefined) {
    return "";
  }
  var actions = [];
  Object.keys(datas).some((key) => {
    if (datas[key].dictValue == ('' + label)) {
      actions.push(datas[key].dictKey);
      return true;
    }
  })
  if (actions.length === 0) {
    actions.push(value);
  }
  return actions.join('');
}
//将字典转化为只有显示的数组名称
export function dictToArray(datas){
	var actions = [];
	Object.keys(datas).some((key) => {
	   actions.push(datas[key].dictValue);
	})
	return actions;
}
export function toComponetData(datas,label,value){
	var list=[]
	datas.forEach((item) => {
		let json={}
		json[label]=item.dictValue
		json[value]= item.dictKey
		list.push(json);
		
	})
	return list;
}
export function getOptionLabel(data,val){
	let label='';
	data.forEach((item) => {
		if(item.value===val){
			label=item.label;
			return ;
		}
	})
	if(label!=null && label!='' && label!=undefined){
		if(label.length>5){
			label=label.substring(0,5)+"...";
		}
	}
	return label;
}
export function getActiveVal(data){
	let val=[];
	data.forEach((item) => {
		if(item.active===true){
			val.push(item.value);
		}
	})
	return val;
}
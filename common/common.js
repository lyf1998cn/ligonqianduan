/**
 * 通用js方法封装处理
 * Copyright (c) 2019 aidex
 */

export function replaceAll (text,stringToFind,stringToReplace) {
	   if ( stringToFind == stringToReplace) return this;
	          var temp = text;
	          var index = temp.indexOf(stringToFind);
	          while (index != -1) {
	              temp = temp.replace(stringToFind, stringToReplace);
	              index = temp.indexOf(stringToFind);
	          }
	return temp;
}
export const loadStyle = url => {
  const link = document.createElement('link');
  link.type = 'text/css';
  link.rel = 'stylesheet';
  link.href = url;
  const head = document.getElementsByTagName('head')[0];
  head.appendChild(link);
};
// 记录路由地址
export const fungoPreviousPage = () => {
	let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
	if(routes.length<=0) return ;
	let curRoute = routes[routes.length - 1].route //获取当前页面路由
	let curParam = routes[routes.length - 1].options; //获取路由参数
	// 拼接参数
	let param = ''
	for (let key in curParam) {
		if (!param) {
			param += key + '=' + curParam[key]
		} else {
			param += '&' + key + '=' + curParam[key]
		}
	}
	// 如果是首页则不记录
	if (curRoute != '/pages/sys/workbench/index') {
		let redirectUrl='/' + curRoute;
		if(param){
			redirectUrl=redirectUrl+"?"+param;
		}
		uni.setStorageSync('redirect',redirectUrl);
	}
}
export const goPage = () => {
	var redirect = uni.getStorageSync('redirect')
	if (redirect) {
		//删除url记录避免重复跳转
		uni.removeStorageSync('redirect')
		uni.reLaunch({
			url: redirect
		})
	}else{
		uni.reLaunch({
			url: '/pages/sys/workbench/index'
		});
	}
}


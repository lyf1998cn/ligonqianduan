/**
 * Copyright (c) 2013-Now http://aidex.vip All rights reserved.
 */
const config = {

	// 产品名称
	productName: 'uniapp',

	// 公司名称
	companyName: '福建理工大学',

	// 产品版本号
	productVersion: 'V1.0.6',

	// 版本检查标识
	appCode: 'android',

	// 内部版本号码
	appVersion: '1.0.6',

	// 管理基础路径
	adminPath: ''

}

// 设置后台接口服务的基础地址
config.baseUrl = '/api';
config.clientId = 'uniapp';
config.clientSecret = 'uniapp_secret';
config.authUrl = 'https://apps.fjut.edu.cn/api/blade-auth/oauth/render';
config.serverUrl = 'https://nids.fjut.edu.cn/authserver/oauth2.0/authorize';
config.appUrl = 'https://apps.fjut.edu.cn/h5/';
config.source = "cas_ticket";
config.socialSource="wechat_mp";

//默认头像地址
config.defaultAvatar = '/static/aidex/images/user01.png';
config.tenantId = '000000';
config.switchMode = true;
export default config;

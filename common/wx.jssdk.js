export function invokeWxJsApi(jsApiList = [], invokeJsApiFuncCallBack, callBackParam = {}) {
	let url = window.location.href.split('#')[0];
	// console.log("当前网页的url:%s", url);
	let params = {
		url: url
	};
	//待考虑是否读取本地缓存

	//从后台读取配置信息
	uni.$u.api.wechat.wechatJsSdkService.getGlobalWxSignatureConfig(params).then(res => {
		if (res.code == 200) {
			let config = res.data.config;
			let corpID = config.corpId;
			let timestamp = config.timestamp;
			let nonceStr = config.nonceStr;
			let signature = config.signature;
			uni.fjutWeixin.config({
				beta: true, // 必须这么写，否则wx.invoke调用形式的jsapi会有问题
				debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId: corpID, // 必填，企业微信的corpID，必须是本企业的corpID，不允许跨企业使用
				timestamp: timestamp, // 必填，生成签名的时间戳
				nonceStr: nonceStr, // 必填，生成签名的随机串
				signature: signature, // 必填，签名，见 附录-JS-SDK使用权限签名算法
				jsApiList: ['agentConfig'].concat(jsApiList) // 必填，需要使用的JS接口列表，凡是要调用的接口都需要传进来
			});
			uni.fjutWeixin.ready(function() {
				// config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，
				// config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
				console.log("wx.ready...");
				callWxAgentConfig(res.data.agentConfig, jsApiList, invokeJsApiFuncCallBack, callBackParam);
			});
			uni.fjutWeixin.error(function(res) {
				// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
				console.error('config error', res);
			});
		} else {
			uni.showToast({
				title: res.msg,
				icon: "none"
			});
		}
	});

	function callWxAgentConfig(agentConfig, jsApiList = [], invokeJsApiFuncCallBack, callBackParam) {
		let corpID = agentConfig.corpId;
		let timestamp = agentConfig.timestamp;
		let nonceStr = agentConfig.nonceStr;
		let signature = agentConfig.signature;
		let agentid = agentConfig.agentId;
		uni.fjutWeixin.agentConfig({
			corpid: corpID, // 必填，企业微信的corpid，必须与当前登录的企业一致
			agentid: agentid, // 必填，企业微信的应用id （e.g. 1000247）
			timestamp: timestamp, // 必填，生成签名的时间戳
			nonceStr: nonceStr, // 必填，生成签名的随机串
			signature: signature, // 必填，签名，见附录-JS-SDK使用权限签名算法
			jsApiList: jsApiList, //必填，传入需要使用的接口名称
			success: function(res) {
				// 回调
				console.log("wx.agentConfig success", JSON.stringify(res));
				if (res.errMsg == "agentConfig:ok") {
					if (isFunction(invokeJsApiFuncCallBack)) {
						// 回调invokeJsApiFuncCallBack，如：调用快速会议等
						invokeJsApiFuncCallBack(callBackParam);
					} else {
						console.error("invokeJsApiFuncCallBack不是函数", invokeJsApiFuncCallBack);
					}
				}
			},
			fail: function(res) {
				console.error("wx.agentConfig fail", JSON.stringify(res));
				if (res.errMsg.indexOf('function not exist') > -1) {
					alert('版本过低请升级')
				}
			}
		});
	}

	function isFunction(obj) {
		return typeof obj === 'function' && typeof obj.nodeType !== "number";
	}
}

export function isAllowAlert() {
	// 是否开启手机端的输出调试
	let isAlert = false;
	return isAlert;
}
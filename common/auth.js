export function setToken (vm,data) {
   vm.$u.vuex('vuex_userName', data.user_name);
   vm.$u.vuex('vuex_accessToken', data.access_token);
   vm.$u.vuex('vuex_refreshToken', data.refresh_token);
   vm.$u.vuex('vuex_userId', data.user_id);
   vm.$u.vuex('vuex_account', data.account);
   vm.$u.vuex('vuex_realName', data.real_name);
   vm.$u.vuex('vuex_nickName', data.nick_name);
   vm.$u.vuex('vuex_avatar', data.avatar);
   vm.$u.vuex('vuex_deptId', data.dept_id);
   vm.$u.vuex('vuex_roleId', data.role_id);
   vm.$u.vuex('vuex_postId', data.post_id);
   vm.$u.vuex('vuex_locale', data.locale);
   vm.$u.vuex('vuex_isAgent', data.isAgent);
   vm.$u.vuex('vuex_expiresIn',parseInt(new Date().getTime())+data.expires_in*1000);
   vm.$u.vuex('vuex_loginSource',data.detail.loginSource);
   vm.$u.vuex('vuex_personType',data.detail.personType);
   vm.$u.vuex('vuex_roleName',data.role_name);
}
export function removeToken (vm) {
   vm.$u.vuex('vuex_userName', '');
   vm.$u.vuex('vuex_accessToken', '');
   vm.$u.vuex('vuex_refreshToken', '');
   vm.$u.vuex('vuex_userId', '');
   vm.$u.vuex('vuex_account', '');
   vm.$u.vuex('vuex_realName', '');
   vm.$u.vuex('vuex_nickName', '');
   vm.$u.vuex('vuex_avatar', '');
   vm.$u.vuex('vuex_deptId', '');
   vm.$u.vuex('vuex_roleId', '');
   vm.$u.vuex('vuex_postId', '');
   vm.$u.vuex('vuex_locale', '');
   vm.$u.vuex('vuex_isAgent', '');
   vm.$u.vuex('vuex_expiresIn','');
   vm.$u.vuex('vuex_loginSource','');
   vm.$u.vuex('vuex_personType','');
   vm.$u.vuex('vuex_roleName','');
}
export function setPermission(vm){
	vm.$u.api.menu.getButton().then(res => {
		let permission=res.data;
		let result = [];
		function getCode(list) {
			list.forEach(ele => {
			  if (typeof (ele) === 'object') {
				const chiildren = ele.children;
				const code = ele.code;
				if (chiildren) {
				  getCode(chiildren)
				} else {
				  result.push(code);
				}
			  }
			})
		}
		getCode(permission);
		let list=[];
		vm.$u.vuex('vuex_permission','');
		let permissionObj={};
		result.forEach(ele => {
			permissionObj[ele] = true;
		});
		vm.$u.vuex('vuex_permission',permissionObj);
	});
}
export function hasPermission(code) {
	let flag=false;
	if(this.vuex_permission){
		Object.keys(this.vuex_permission).some((key) => {
		  if(key===code){
			  flag=this.vuex_permission[key];
			  return ;
		  }
		})
	}
	return flag;
}

export function isWechatMpLogin() {
	let isWechatMp = this.vuex_loginSource === '2';
	return isWechatMp;
}

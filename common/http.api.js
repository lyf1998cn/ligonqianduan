/**
 * Copyright (c) 2013-Now http://aidex.vip All rights reserved.
 */
// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作
const install = (Vue, vm) => {

	// 参数配置对象
	const config = vm.vuex_config;

	// 将各个定义的接口名称，统一放进对象挂载到vm.$u.api(因为vm就是this，也即this.$u.api)下
	vm.$u.api = {
		// 基础服务：登录登出、身份信息、菜单授权、切换系统、字典数据等
		login: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-auth/oauth/token', params),
		//单点登录
		casLogin: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-auth/oauth/token', params),
		logout: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-auth/oauth/logout', params,{authorization: false}),
		refreshToken: (params = {},headers ={}) => vm.$u.post(config.adminPath+'/api/blade-auth/oauth/token', params,headers),
		dictBizData: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/dict-biz/dictionary', params),
		dictData: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/dict/dictionary', params),
		index: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-auth/oauth/index', params,{authorization: false}),
		getUrlData: (url,params = {},requestType = 'get') => {
			if(requestType === 'get') {
				return vm.$u.get(config.adminPath+url, params);
			}else if(requestType === 'post') {
				return vm.$u.post(config.adminPath+url, params);
			}
		},
		dept: {
			detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/deptExt/deptInfo', params),
			deptUserLazyTree: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/deptExt/dept-user-lazy-tree', params),
			lazyList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/dept/lazy-list', params)
		},
		role: {
			selectRole: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/role/select', params)
		},
		user: {
			getUserInfo: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/user/info', params),
			list: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/user/phone-list', params),
			detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/user/phone-detail', params),
			getUserListByClass:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/user/user-list-by-class-mobile', params),
			chooseUserPage:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/user/choose-user-page', params),
			getZyDictData:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/zyCode/listAll', params),
			getBjDictData:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/ssbjCode/listAll', params),
		},
		activeWechat:{
			getUserInfoByOpenId: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-auth/oauth/getOpenId', params,{authorization: false}),
			submitToWechat: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-auth/oauth/submitToWechat', params,{authorization: false}),
		},
		notice: {
			list: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-desk/notice/list', params),
			detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-desk/notice/detail', params)
		},
		rotation: {
			list: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/rotation/homeList', params)
		},
		menu: {
			getRoute: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/menuphone/routes', params),
			getButton: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/menu/buttons', params),
			getDictByCode: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/menu/getDictByCode', params),
		},
		commonMenu: {
			save:(params = {}) => vm.$u.post(config.adminPath+'/api/blade-system/commonMenuSetting/submit', params),
		},
		rssoffer:{
			offerTypeList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/rssofferType/allList', params),
			offerList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/rssoffer/list', params),
      getLatestNews: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/rssoffer/latest-news', params),
      getLatestTodo: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/latest-todo', params),
		},
		yanjiusheng:{
			yjscjList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/yjsCj2Apps/list', params),
			yjsKkxxList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/yjsKkxx2Apps/list', params),
			yjsXsfblwList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/yjsXsfblw2Apps/list', params),
			yjsXskbList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/yjsXskb2Apps/list', params),
		},
		yikatong:{
			yktGrjyxxList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/yktGrjyxx2Apps/list', params),
			yktShxxList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/yktShxx2Apps/list', params),
		},
		jiaoxue:{
			jwBzksKcxxList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/jwBzksKcxx2Apps/list', params),
			jwBzksKscjList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/jwBzksKscj2Apps/list', params),
			jwCxcyxfList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/jwCxcyxf2Apps/list', params),
			jwJzgRkxxList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/jwJzgRkxx2Apps/list', params),
		},
		keyan:{
			kynhjList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/kynCghj2Apps/list', params),
			kynlwList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/kynLw2Apps/list', params),
			kynzlList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/kynZl2Apps/list', params),
			kynzzList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/kynZz2Apps/list', params),
			kynFzxmList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/kynKyxm2Apps/fzkyList', params),
			kynCyxmList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/kynKyxm2Apps/cykyList', params),
		},
		xuegong:{
			xgBzksJlList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/xgBzksJl2Apps/list', params),
			xgBzksRychList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/xgBzksRych2Apps/list', params),
		},
		tushu:{
			tsJylsList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/tsJyls2Apps/list', params),
			tsWzxxList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/tsWzxx2Apps/list', params),
		},
		message:{
			msgTemplate:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgTemplate/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgTemplate/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-message/msgTemplate/submit', params),
				delete: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-message/msgTemplate/remove', params),
				templateDict:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgTemplate/templateList', params),
			},
			msgHolidyMsg:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgHolidyMsg/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgHolidyMsg/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-message/msgHolidyMsg/submit', params),
				delete: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-message/msgHolidyMsg/remove', params),
			},
			msgReceiveBatch:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgReceiveBatch/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgReceiveBatch/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-message/msgReceiveBatch/submit', params),
				delete: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-message/msgReceiveBatch/remove', params),
				viewDetail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgReceiveBatch/viewDetail', params),
			},
			msgGroup:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgGroup/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgGroup/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-message/msgGroup/submit', params),
				delete: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-message/msgGroup/remove', params),
				groupDict:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgGroup/dict', params),
			},
			msgGroupUser:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgGroupUser/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgGroupUser/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-message/msgGroupUser/submit', params),
				delete: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-message/msgGroupUser/remove', params),
				ownerList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgGroupUser/owner-list', params)
			},
			msgSendRecord:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgSendRecord/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgSendRecord/detail', params),
			},
			msgReplyRecord:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgReplyRecord/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-message/msgReplyRecord/detail', params),
			}
		},
		mis:{
			lostAndFound: {
				list: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/mislostfound/list', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/mislostfound/submit', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/mislostfound/detail', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-mis/mislostfound/remove', params)
			},
			misAssistantTalk:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misAssistantTalk/submit', params),
				submitRate:(params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misAssistantTalk/submitRate', params),
				submitFeedBack:(params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misAssistantTalk/feedBackRate', params),
				listRateByTalkId:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/list-by-talk-id', params),
				queryStaticByDept:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/query-static-by-dept', params),
				queryStaticByAssistant:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/query-static-by-assistant', params),
				queryUserTalkCnt:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/query-user-talk-cnt', params),
				rateList:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantTalk/rate-list', params),
			},
			misAssistantTalkCoach:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/misAssistantTalkCoach/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-system/misAssistantTalkCoach/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-system/misAssistantTalkCoach/submit', params),
				grantAssistant: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-system/user/grant-assistant', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-system/misAssistantTalkCoach/remove', params),
			},
			misSchoolCalendar:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misSchoolCalendar/list', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misSchoolCalendar/submit', params),
				update: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misSchoolCalendar/update', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misSchoolCalendar/detail', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-mis/misSchoolCalendar/remove', params),
				changeStatus: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-mis/misSchoolCalendar/changeStatus', params),
				getDict:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misSchoolCalendar/dict', params),
			},
			misSchoolCalendarEvent:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misSchoolCalendarEvent/list', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misSchoolCalendarEvent/submit', params),
				update: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misSchoolCalendarEvent/update', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misSchoolCalendarEvent/detail', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-mis/misSchoolCalendarEvent/remove', params)
			},
			tcTask:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/task/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/task/detail', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-mis/task/remove', params)
			},
			misAssistantUserExt:{
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantUserExt/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-mis/misAssistantUserExt/detail', params),
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-mis/misAssistantUserExt/submit', params),
			},
		},
		wechat:{
			wechatGroup:{
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatGroup/submit', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatGroup/remove', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatGroup/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatGroup/detail', params),
				enterGroup: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatGroup/enterGroup', params),
				quitGroup: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatGroup/quitGroup', params),
				delGroupUser: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatGroup/delGroupUser', params),
				getGroupList4CurrentUser: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatGroup/dict', params)
			},
			wechatGroupNotice: {
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatGroupNotice/submit', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatGroupNotice/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatGroupNotice/detail', params)
			},
			wechatMeeting:{
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatMeeting/save', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatMeeting/remove', params),
				edit: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatMeeting/update', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatMeeting/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatMeeting/detail', params),
				cancel: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatMeeting/updateCancel', params)
			},
			wechatLiving:{
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatLiving/submit', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatLiving/remove', params),
				edit: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatLiving/submit', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatLiving/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatLiving/detail', params),
				cancel: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatLiving/updateCancel', params),
				submitNotSyncWeiXin: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatLiving/submitNotSyncWeiXin', params),
			},
			wechatAgent: {
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatAgent/submit', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatAgent/remove', params),
				edit: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatAgent/update', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatAgent/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatAgent/detail', params),
				getAgentAllDict: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatAgent/alldict', params)
			},
			wechatAgentNotice: {
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatAgentNotice/submit', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatAgentNotice/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatAgentNotice/detail', params),
				recall: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatAgentNotice/recall', params),
				wechatTagTree: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatTag/tree', params)
			},
			wechatSchedule: {
				save: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatSchedule/submit', params),
				remove: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatSchedule/remove', params),
				list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatSchedule/list', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatSchedule/detail', params),
				cancel: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatSchedule/cancel', params),
				myScheduleList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatSchedule/myScheduleList', params),
				convergeMyScheduleList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatSchedule/convergeMyScheduleList', params)
			},
			wechatJsSdkService: {
				getGlobalWxSignatureConfig: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-wechat/wechatJsSdk/getGlobalWxSignatureConfig', params)
			},
			wechatTag: {
				list: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatTag/page', params),
			},
			wechatSigninUser: {
				list: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatSigninUser/list', params),
				save: (params = {}) => vm.$u.post(config.adminPath+'/api/blade-wechat/wechatSigninUser/submit', params),
				detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-wechat/wechatSigninUser/detail', params),
			}
		},
		wflow: {
			deploy: {
				getLastedVersionPublish: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/deploy/getLastedVersionPublish', params),
			},
			process: {
				listProcess: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/list', params),
				getProcessDeployForm: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/getProcessDeployForm', params),
				startProcess: (procDefId,params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/workflow/process/start/' + procDefId, params),
				detailProcess: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/detail', params),
				listOwnProcess: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/ownList', params),
				todoList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/todoList', params),
				ownList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/ownList', params),
				finishedList: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/finishedList', params),
				todoCount: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/process/getTodoCount', params),
			},
			task: {
				submitFlow: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/workflow/task/submitFlow', params),
				stopProcess: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/workflow/task/stopProcess', params),
				revokeProcess: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/workflow/task/revokeProcess', params),
			},
			business: {
				visitor: {
					getProcessVisitorConf: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitor/getProcessVisitorConf', params),
					getCandidateUsers: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/getCandidateUsers', params),
					getVisitorConfAndLastedDeploy: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitor/getVisitorConfAndLastedDeploy', params),
					list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitor/list', params),
					detail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitor/detail', params),
				},
				visitorOuterNetApi: {
					selectVisitorBaseInfoByOpenId: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/selectVisitorBaseInfoByOpenId', params),
					detailGatePass: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/detailGatePass', params),
					list:(params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/list', params),
					getVisitorConfAndLastedDeploy: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/getVisitorConfAndLastedDeploy', params),
					dictionaryEnterCampus: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/dictionaryEnterCampus', params),
					dictionaryVisitorIdType: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/dictionaryVisitorIdType', params),
					downloadFileAvatarImage: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/download-file-avatar-image', params),
					getCandidateUsersByPhone: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/getCandidateUsersByPhone', params),
					submitFlow: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/submitFlow', params),
					detailProcess: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/detailProcess', params),
					stopProcess: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/processVisitorOuterNetApi/stopProcess', params),
				},
			},
			draft: {
				getDraftDetail: (params = {}) => vm.$u.get(config.adminPath+'/api/blade-flowable-plus/workflow/draft/detail', params),
				addDraft: (params = {}) => vm.$u.postJson(config.adminPath+'/api/blade-flowable-plus/workflow/draft/submit', params),
			},
		},

		lang: (params = {}) => vm.$u.get('/lang/'+params.lang),

		// sendCode: (params = {}) => vm.$u.post(config.adminPath+'/mobile/login/sendCode', params),
		// registerUser: (params = {}) => vm.$u.post(config.adminPath+'/mobile/user/registerUser', params),
		// //首页相关api
		// getIndexCardInfo: (params = {}) => vm.$u.get(config.adminPath+'/mobile/index/getIndexCardInfo', params),
		// getM2mOrderFlowList: (params = {}) => vm.$u.get(config.adminPath+'/mobile/index/getM2mOrderFlowList', params),
		// //获取卡可购买套餐包
		// getM2mOrderPackageList: (params = {}) => vm.$u.get(config.adminPath+'/mobile/index/getM2mOrderPackageList', params),


		// authInfo: (params = {}) => vm.$u.get(config.adminPath+'/authInfo', params),
		// menuTree: (params = {}) => vm.$u.get(config.adminPath+'/menuTree', params),
		// switchSys: (params = {}) => vm.$u.get(config.adminPath+'/switch/'+params.sysCode),

		// // 账号服务：验证码接口、忘记密码接口、注册账号接口等
		// validCode: (params = {}) => vm.$u.getText('/validCode', params),
		// getFpValidCode: (params = {}) => vm.$u.post('/account/getFpValidCode', params),
		// savePwdByValidCode: (params = {}) => vm.$u.post('/account/savePwdByValidCode', params),
		// getRegValidCode: (params = {}) => vm.$u.post('/account/getRegValidCode', params),
		// saveRegByValidCode: (params = {}) => vm.$u.post('/account/saveRegByValidCode', params),

		// // APP公共服务
		// upgradeCheck: () => vm.$u.post('/app/upgrade/check', {appCode: config.appCode, appVersion: config.appVersion}),
		// commentSave: (params = {}) => vm.$u.post('/app/comment/save', params),

		// // 个人信息修改
		// user: {
		// 	saveUserInfo: (params = {}) => vm.$u.post(config.adminPath+'/mobile/user/saveUserInfo', params),
		// 	infoSavePwd: (params = {}) => vm.$u.put(config.adminPath+'/system/user/profile/updatePwd', params),
		// 	infoSavePqa: (params = {}) => vm.$u.post(config.adminPath+'/sys/user/infoSavePqa', params),
		// },

		// 员工用户查询
		// empUser: {
		// 	listData: (params = {}) => vm.$u.get(config.adminPath+'/sys/empUser/listData', params),
		// },

		// // 组织机构查询
		// office: {
		// 	treeData: (params = {}) => vm.$u.get(config.adminPath+'/sys/office/treeData', params),
		// },

		// // 增删改查例子
		// testData: {
		// 	form: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/form', params),
		// 	list: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/listData', params),
		// 	save: (params = {}) => vm.$u.postJson(config.adminPath+'/test/testData/save', params),
		// 	disable: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/disable', params),
		// 	enable: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/enable', params),
		// 	delete: (params = {}) => vm.$u.post(config.adminPath+'/test/testData/delete', params),
		// },

	};

}

export default {
	install
}

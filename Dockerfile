FROM nginx
VOLUME /tmp
ENV LANG en_US.UTF-8
ADD ./unpackage/dist/build/h5/ /usr/share/nginx/html/
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
